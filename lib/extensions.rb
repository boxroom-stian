class String
    # short helping function to shorten a text like this Sti...klev
  def shorten(length)
    txt = self
    if self.length > length
      side = ( length/2 ) - 2
      txt =(self[0..side] + "..." + self[-side..-1])
    end
    return txt
  end
end