#!/bin/bash
mkdir -p data/development/uploads
mkdir -p data/development/trash
mkdir -p data/testing/uploads
mkdir -p data/testing/trash
mkdir -p data/production/uploads
mkdir -p data/production/trash
mkdir -p data/production/temp
mkdir -p data/development/temp
mkdir -p data/testing/temp
mkdir log
mkdir tmp
