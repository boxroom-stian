# Be sure to restart your web server when you modify this file.

# Uncomment below to force Rails into production mode when 
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '1.2.2' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here
  
  # Skip frameworks you're not going to use (only works if using vendor/rails)
  # config.frameworks -= [ :action_web_service, :action_mailer ]

  # Only load the plugins named here, by default all plugins in vendor/plugins are loaded
  # config.plugins = %W( exception_notification ssl_requirement )

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )

  # Force all environments to use the same logger level 
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Use the database for sessions instead of the file system
  # (create the session table with 'rake db:sessions:create')
  # config.action_controller.session_store = :active_record_store

  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper, 
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector

  # Make Active Record use UTC-base instead of local time
  # config.active_record.default_timezone = :utc
  
  # See Rails::Configuration for more options
end

# Add new inflection rules using the following format 
# (all these examples are active by default):
# Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf
# Mime::Type.register "application/x-mobile", :mobile

# Include your application configuration below

# ActionMailer config:
ActionMailer::Base.delivery_method = :smtp

ActionMailer::Base.smtp_settings = {
  :address => 'localhost',
  :port => 25,
  :domain => 'localhost'
}

ExceptionNotifier.exception_recipients = ["root@localhost"]

# Path where the files will be stored
UPLOAD_PATH = "data/#{RAILS_ENV}/uploads"
TRASH_PATH = "data/#{RAILS_ENV}/trash"
TEMP_PATH = "data/#{RAILS_ENV}/temp"
FERRET_PATH = "index/#{RAILS_ENV}"   # currently not used, couldn't get it to work
TITLE = 'Intranet'

INTRANET_MENU = "
<a href='http://example1.com'>Link 1</a>
<a href='http://example2.com'>Link 2</a>"

VALID_EMAIL = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/

# Use upload progress (or not)
USE_UPLOAD_PROGRESS = false

# We need acts_as_ferret
require 'acts_as_ferret'

require 'lib/extensions.rb'

FILE_TYPES_BLOCKED = /\.(mp3|avi|mpg|mp4|exe|com|bat)$/i

FILE_ICONS = [
  { :ext => /\.pdf$/i, :icon => "pdf.png"},
  { :ext => /\.(doc|rtf|sxw|odt)$/i, :icon => "word.png"},
  { :ext => /\.txt$/i, :icon => "txt.png"},
  { :ext => /\.(jpg|jpeg|tiff|png|gif)$/i, :icon => "image.png"},
  { :ext => /\.xls$/i, :icon => "spreadsheet.png"},
  { :ext => /\.htm[l]?$/i, :icon => "fileicon-html.png"},
  { :ext => /\.exe$/i, :icon => "fileicon-exe.png"},
  { :ext => /\.(zip|tar|tgz|gzip|gz|bz2|7z)$/i, :icon => "compressed.png"},
  ]
  
# Define the helpers that extract the plain-text to be indexed
INDEX_HELPERS = [ # defines helpers
  # Examples:
  { :ext => /\.htm[l]?$/i, :helper => 'html2text %s'},
  { :ext => /\.ppt$/i, :helper => 'catppt %s', :remove_after => /Created with xlhtml/ },
  { :ext => /\.rtf$/i, :helper => 'unrtf --text %s', :remove_before => /-----------------/ },
  { :ext => /\.doc$/i, :helper => 'wvText %s %s', :file_output => true },
  { :ext => /\.pdf$/i, :helper => 'pdftotext %s -', :remove_before => /No paper information available - using defaults/ },
  { :ext => /\.zip$/i, :helper => 'unzip -l %s'},
]
