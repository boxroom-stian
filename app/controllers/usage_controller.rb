class UsageController < ApplicationController
  def show
    @pages, @usages = paginate_collection(:per_page => 30, :page => params[:page]) do 
      if params[:id] 
        @name = (params[:file] ? Myfile.find(params[:id]).filename : Folder.find(params[:id]).name )
        (params[:file] ? Myfile : Folder).find(params[:id]).usages 
      else
        Usage.find(:all,:order => "download_date_time desc")
      end
    end    
  end
  
end