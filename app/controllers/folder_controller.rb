# The folder controller contains the following actions:
# [#index]              the default action, redirects to list
# [#list]               list files and sub folders in a folder
# [#feed]               authorizes, sets appropriate variables and header for RSS feed
# [#feed_warning]       renders page with explanations/warnings about RSS feed
# [#new]                shows the form for creating a new folder
# [#create]             create a new folder
# [#rename]             show the form for adjusting the folder's name
# [#update]             updates the attributes of a folder
# [#destroy]            delete a folder
# [#update_permissions] save the new rights given by the user

class FolderController < ApplicationController
  skip_before_filter :authorize, :only => :feed

  before_filter :does_folder_exist, :except => [:list, :feed, :feed_warning]
  before_filter :authorize_creating, :only => [:new, :create, :multimove]
  before_filter :authorize_reading, :only => :list
  before_filter :authorize_updating, :only => [:rename, :update, :update_rights]
  before_filter :authorize_deleting, :only => :destroy

  # Sessions are not needed for feeds
  session :off, :only => 'feed'
  layout 'folder', :except => 'feed'
                                   
  # do something to selected files in folder list
  def multichange
    @folder = Folder.find(folder_id)
    unless @logged_in_user.can_delete(folder_id)
      flash[:folder_error] = "You don't have delete permissions for this folder."
      redirect_to :action => 'list', :id => folder_id and return false
    end

    checked_files, checked_folders = [], []
    params['checked_file'].each {|k,v| checked_files << Myfile.find(k.to_i) if v == 'yes' } if params['checked_file']
    params['checked_folder'].each {|k,v| checked_folders << Folder.find(k.to_i) if v == 'yes' } if params['checked_folder']
    unless (checked_files.size + checked_folders.size) > 0
      flash[:folder_error] = "You didn't select any files or folders."
      redirect_to :action => 'list', :id => folder_id and return false
    end

    case params['checked']['action']
      when 'delete'
        checked_files.each {|x| x.destroy }
        checked_folders.each {|x| x.delete }

      when 'add to clipboard'
        flash[:folder_info] = "The files and/or folders you marked have been put on the clipboard. They will not disappear from this folder, until you choose moving them to a new folder."
        temp = @logged_in_user.clipboards
        already_files, already_folders = temp.collect(&:myfile), temp.collect(&:folder)
        checked_files.each do |x| 
          unless already_files.index(x)
            Clipboard.new(:user => @logged_in_user, :myfile => x).save 
          end
        end
        
        checked_folders.each do |x| 
          unless already_folders.index(x) 
            Clipboard.new(:user => @logged_in_user, :folder => x).save 
          end
        end

      when 'download all'
        tmpfile = TEMP_PATH + "/zip" + Time.now.to_f.to_s + ".tmp"
        folders, files = "", "" 
        checked_folders.collect(&:name).each {|x| folders << "\"#{x}\" " }
        checked_files.collect(&:filename).each {|x| files << "\"#{x}\" " }

        # tricky because we don't want absolute paths in the zip file, but the temp 
        # dir is a relative path 
        slashes = @folder.path_on_disk.scan('/').size
        slashes += 1 unless @folder.id == 1
        tmppath = '../' * slashes
        cmd = "cd \"#{@folder.path_on_disk}\"; zip -r #{tmppath}#{tmpfile} #{folders} #{files}"
        `#{cmd}`   
        puts cmd
        p files
        @folder.log_usage("zipped", cmd)
        p @folder.path_on_disk

        if File.exists?(tmpfile)
          send_file tmpfile, :filename => "DownloadAllFiles.zip"
          return false
        else
          flash[:folder_error] = "Could not zip selected files."
          @folder.log_usage("error","could not zip " + checked_folders.join(":") + " " + checked_files.join(":") )
        end
          
    end
    redirect_to :action => 'list', :id => folder_id
  end

  # move files and folders in clipboard to the current folder
  def multimove
    @logged_in_user.clipboards.each do |x|
      if x.folder
        File.mv(x.folder.path_on_disk, @folder.path_on_disk)
        x.folder.log_usage("moved", "from #{x.folder.name} to #{@folder.name}")
        x.folder.parent = @folder
        x.folder.save
      else
        File.mv(File.join(x.myfile.folder.path_on_disk, x.myfile.filename), @folder.path_on_disk)
        x.myfile.log_usage("moved", "from #{x.myfile.folder.name} to #{@folder.name}")
        x.myfile.folder = @folder
        x.myfile.save
      end
      x.destroy
    end
    redirect_to :action => 'list', :id => folder_id
  end

  # deletes all clipboard entries for a given user
  def cancel_moving
    @logged_in_user.clipboards.collect(&:destroy)
    redirect_to :action => 'list', :id => params[:folder_id]
  end

  # The default action, redirects to list.
  def index
    list
    render_action 'list'
  end

  # List the files and sub-folders in a folder.
  def list
    # Get the folder
    @folder = Folder.find_by_id(folder_id)
    
    # Set if the user is allowed to update or delete in this folder;
    # these instance variables are used in the view.
    @can_update = @logged_in_user.can_update(@folder.id)
    @can_delete = @logged_in_user.can_delete(@folder.id)

    # determine the order in which files are shown
    file_order = 'filename '
    file_order = params[:order_by].sub('name', 'filename') + ' ' if params[:order_by]
    file_order += params[:order] if params[:order]

    # determine the order in which folders are shown
    folder_order = 'name '
    if params[:order_by] and params[:order_by] != 'filesize'    
      folder_order = params[:order_by] + ' '
      folder_order += params[:order] if params[:order]
    end

    # List of subfolders
    @folders = @folder.list_subfolders(@logged_in_user, folder_order.rstrip)

    # List of files in the folder
    @myfiles = @folder.list_files(@logged_in_user, file_order.rstrip)

    #get the correct URL
    url = url_for(:controller => 'folder', :action => 'list', :id => nil)

    # it's nice to have the possibility to go up one level
    @folder_up = @folder.parent.id.to_s if @folder.parent
  end

  # Authorizes, sets the appropriate variables and headers.
  # The feed is actually implemented in: app/views/folder/feed.rxml.
  def feed
    # check for valid access key:
    user = User.find_by_rss_access_key(params[:access_key])
    @authorized = !user.blank?

    # get the folder
    @folder = Folder.find_by_id(folder_id)

    # set appriopriate instance variables,
    # so the feed can be created in folder.rxml
    if @authorized and not @folder.blank?
      if @folder.is_root or user.can_read(@folder.id)
        @folders = @folder.list_subfolders(user, 'name')
        @myfiles = @folder.list_files(user, 'filename')
      else
        @authorized = false
      end
    end

    # finally, set correct header
    if @authorized
      headers['Content-Type'] = 'text/xml'
    else
      headers['Content-Type'] = 'text/html'
    end
  end

  # Page that shows warning about RSS
  # and the feed's authorization.
  def feed_warning
    render
  end

  # Shows the form where a user can enter the name for the a folder.
  # The new folder will be stored in the 'current' folder.
  def new
    @folder = Folder.new
  end

  # Create a new folder with the posted variables from the 'new' view.
  def create
    if request.post?
      params[:folder][:name] = Myfile.base_part_of(params[:folder][:name])
      @folder = Folder.new(params[:folder])
      @folder.parent = Folder.find(folder_id)
      @folder.date_modified = Time.now
      @folder.user = @logged_in_user
      if @folder.save
        # copy groups rights on parent folder to new folder
        copy_permissions_to_new_folder(@folder)
        # back to the list
        redirect_to :action => 'list', :id => params[:id]
      else
        render_action 'new'
      end
    end
  end

  # Show a form with the current name of the folder in a text field.
  def rename
    render
  end

  # Update the folder attributes with the posted variables from the 'rename' view.
  def update
    if request.post?
      if @folder.rename(params[:folder][:name])
        redirect_to :action => 'list', :id => folder_id
      else
        render_action 'rename'
      end
    end
  end

  # Delete a folder.
  def destroy
    @folder.delete
    redirect_to :action => 'list', :id => folder_id
  end

  # Saved the new permissions given by the user
  def update_permissions
    if request.post?
      @folder = Folder.find(folder_id)
      if @logged_in_user.can_update_perms?
        # update the create, read, update, delete right for this folder:
        update_group_permissions(folder_id, params[:create_check_box], 'create', params[:update_recursively][:checked] == 'yes' ? true : false)
        update_group_permissions(folder_id, params[:read_check_box], 'read', params[:update_recursively][:checked] == 'yes' ? true : false)
        update_group_permissions(folder_id, params[:update_check_box], 'update', params[:update_recursively][:checked] == 'yes' ? true : false)
        update_group_permissions(folder_id, params[:delete_check_box], 'delete', params[:update_recursively][:checked] == 'yes' ? true : false)

        # changing name of folder owner
        newuser = User.find_by_name(params[:owner])
        if newuser
          to_change = [@folder]
          to_change += @folder.all_children if params[:owner_recursive][:checked] == 'yes'
          to_change.each do |f|
            puts f
            p "Changing #{f.name}"    
            
            f.user = newuser
            f.save
          end
        else
          flash[:folder_error] = "User #{params[:owner]} could not be found. No change in ownership committed."
        end          

      end
      
      # updating folder info and upload info
      if @logged_in_user.can_update_folderinfo?(@folder)
        @folder.quota = params[:folder][:quota]
        @folder.note = params[:folder][:note]
        @folder.note_upload = params[:folder][:note_upload]
        @folder.note_inheritable = ( params[:folder][:note_inheritable] == '1' ? true : false )
        @folder.note_upload_inheritable = ( params[:folder][:note_upload_inheritable] == '1' ? true : false )
        @folder.save
      end
    end

    # Return to the folder
    redirect_to :action => 'list', :id => folder_id
  end

  # These methods are private:
  # [#update_group_permissions]        Update the group folder permissions
  # [#copy_permissions_to_new_folder]  Copy the GroupPermissions of the parent folder to the given folder
  # [#authorize_reading]               Allows/disallows the current user to read the current folder
  # [#authorize_deleting]              Check logged in user's delete permissions for a particular folder
  # [#authorize_deleting_for_children] Check delete permissions for subfolders recursively
  private
    # Update the group permissions for a given group, folder and field.
    # If <i>recursively</i> is true, update the child folders of the given folder too. 
    def update_group_permissions(folder_id_param, group_check_box_list, field, recursively)
      # iteratively update the GroupPermissions
      group_check_box_list.each do |group_id, can_do_it|
        # get the GroupPermissions
        group_permission = GroupPermission.find_by_group_id_and_folder_id(group_id, folder_id_param)

        # Do the actual update if the GroupPermission exists;
        # do not update the permissions of the admins group
        # (it should always be able to do everything)
        unless group_permission.blank? or group_permission.group.is_the_administrators_group?
          case field
          when 'create':
            group_permission.can_create = can_do_it
          when 'read':
            group_permission.can_read = can_do_it
          when 'update':
            group_permission.can_update = can_do_it
          when 'delete':
            group_permission.can_delete = can_do_it
          end
          group_permission.save
        end
      end

      # The recursive part...
      if recursively
        # Update the child folders
        Folder.find_by_id(folder_id_param).all_children.each do |f|
          update_group_permissions(f, group_check_box_list, field, true)
        end
      end
    end

    # Copy the GroupPermissions of the parent folder to the given folder
    def copy_permissions_to_new_folder(folder)
      # get the 'parent' GroupPermissions
      GroupPermission.find_all_by_folder_id(folder_id).each do |parent_group_permissions|
        # create the new GroupPermissions
        group_permissions = GroupPermission.new
        group_permissions.folder = folder
        group_permissions.group = parent_group_permissions.group
        group_permissions.can_create = parent_group_permissions.can_create
        group_permissions.can_read = parent_group_permissions.can_read
        group_permissions.can_update = parent_group_permissions.can_update
        group_permissions.can_delete = parent_group_permissions.can_delete
        group_permissions.save
      end
    end

    # Redirect to the Root folder and show an error message
    # if current user cannot read in current folder.
    def authorize_reading
      # First check if the folder exists, if it doesn't: show an appropriate message.
      # If the folder does exist, only authorize the read-rights if it's not the Root folder.
      unless Folder.find_by_id(folder_id)
        flash.now[:folder_error] = 'Someone else deleted the folder you are using. Your action was cancelled and you have been taken back to the root folder.'
        redirect_to(:controller => 'folder', :action => 'list', :id => nil) and return false
      else
        super unless folder_id == 1
      end
    end

    # Redirect to the Root folder and show an error message
    # if current user cannot delete in current folder
    def authorize_deleting
      folder = Folder.find_by_id(folder_id)
      unless @logged_in_user.can_delete(folder_id)
        flash.now[:folder_error] = "You don't have delete permissions for this folder."
        redirect_to :controller => 'folder', :action => 'list', :id => folder_id and return false
      else
        authorize_deleting_for_children(folder)
      end
    end

    # Check the delete permissions for all the child folders of the given folder
    def authorize_deleting_for_children(folder)
      folder.children.each do |child_folder|
        unless @logged_in_user.can_delete(child_folder.id)
          error_msg = "Sorry, you don't have delete permissions for one of the subfolders."
          if child_folder.parent.id == folder_id
            flash.now[:folder_error] = error_msg
          else
            flash[:folder_error] = error_msg
          end
          redirect_to :controller => 'folder', :action => 'list', :id => folder_id and return false
        else
          authorize_deleting_for_children(child_folder) # Checks the permissions of a child's children
        end
      end
    end

end
