class FrontpageController < ApplicationController
  session :off, :only => 'feed'
  layout 'frontpage', :except => 'filelist'
  before_filter :is_user_authorized, :except => [:list, :filelist]

  uses_tiny_mce(:options => {:theme => 'advanced',
                             :theme_advanced_buttons1 => %w{bold italic underline strikethrough separator justifyleft justifycenter justifyright separator bullist numlist forecolor backcolor separator link unlink image undo redo},                       
                             :external_link_list_url => "/frontpage/filelist",
                             :convert_newlines_to_brs => true,
                             :theme_advanced_buttons2 => [],
                             :theme_advanced_buttons3 => []})

  def is_user_authorized
    unless @logged_in_user.groups.include?(Group.find_by_name('frontpage'))
      flash.now[:news_error] = 'You are not authorized to edit the news.'
      redirect_to(:controller => 'frontpage', :action => 'list', :id => nil) and return false
    end
  end
                                                                    
  def index
    list
    render :action => 'list'
  end

  def delete
    News.find(params[:id]).destroy
    redirect_to(:controller => 'frontpage', :action => 'list', :id => nil)
  end

  def filelist
    headers['Content-Type'] = 'text/javascript'
    @text = 'var tinyMCELinkList = new Array('
    Group.find_by_name('frontpage').users.each do |user|
      user.clipboards.each do |x|
        if x.folder
          txt = x.folder.path_on_disk[UPLOAD_PATH.size..-1]
          lnk = "/folder/list/#{x.folder.id}"
        else
          txt = x.myfile.folder.path_on_disk[UPLOAD_PATH.size..-1]+ "/" + x.myfile.filename
          lnk = "/file/download/#{x.myfile.id}"
        end
        @text << "[\"#{txt.shorten(60)}\", \"#{lnk}\"],"
      end
    end  
    @text = @text.chop << ");"
  end

  def list
    @news_pages, @news = paginate :news, :per_page => 10, :order => 'date DESC'
  end

  def show
    @news = News.find(params[:id])
  end

  def new
    @news = News.new
  end

  def create
    @news = News.new(params[:news])
    if @news.save
      flash[:notice] = 'News was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @news = News.find(params[:id])
  end

  def update
    @news = News.find(params[:id])
    if @news.update_attributes(params[:news])
      flash[:notice] = 'News was successfully updated.'
      redirect_to :action => 'list'
    else
      render :action => 'edit'
    end
  end

  def destroy
    News.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
