class AdminToolController < ApplicationController

  # uses basic *NIX tools to give quick stats on the server - tested on Linux and OSX
  def server_status
    @status = "<pre><h1>Logged in users and uptime</h1>"
    @status << `w`
    @status << "<h1>Status of disks</h1>"
    @status << `df`
    @status << "<h1>Processes running</h1>"
    @status << `ps ax`.gsub!("<","&lt;")  # even with <pre> this messes up HTML
    @status << "</pre>"
  end

  # shows a pretty printed list of directories and sub-dirs
  def folder_tree
    du_opt = (`uname` == "Darwin\n") ? "d0" : "-summarize"
    @trash_size = `du -#{du_opt} -h #{TRASH_PATH}`.match(/(.*?)\t/)[1] 
    @ferret_size = `du -#{du_opt} -h #{FERRET_PATH}`.match(/(.*?)\t/)[1] 
    du_opt2 = (`uname` == "Darwin\n") ? "d2" : "-max-depth=2"
    @folder_tree = `cd #{UPLOAD_PATH}; du -#{du_opt2} | sort -n`
  end

  def show_log
    @pages, @usages = paginate_collection(:per_page => 30, :page => params[:page]) do 
      if params[:id] 
        @name = (params[:file] ? Myfile.find(params[:id]).filename : Folder.find(params[:id]).name )
        (params[:file] ? Myfile : Folder).find(params[:id]).usages 
      else
        Usage.find(:all,:order => "download_date_time desc")
      end
    end    
  end
  
end