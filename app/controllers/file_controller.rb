# The file controller contains the following actions:
# [#download]          downloads a file to the users system
# [#progress]          needed for upload progress
# [#upload]            shows the form for uploading files
# [#do_the_upload]     upload to and create a file in the database
# [#validate_filename] validates file to be uploaded
# [#rename]            show the form for adjusting the name of a file
# [#update]            updates the name of a file
# [#destroy]           delete files
# [#preview]           preview file; possibly with highlighted search words

class FileController < ApplicationController
  skip_before_filter :authorize, :only => :progress

  before_filter :does_folder_exist, :only => [:upload, :do_the_upload] # if the folder DOES exist, @folder is set to it
  before_filter :does_file_exist, :except => [:upload, :progress, :do_the_upload, :validate_filename,] # if the file DOES exist, @myfile is set to it
  before_filter :authorize_creating, :only => :upload
  before_filter :authorize_reading, :only => [:download, :preview]
  before_filter :authorize_updating, :only => [:rename, :update]
  before_filter :authorize_deleting, :only => :destroy

  session :off, :only => :progress

  # The requested file will be downloaded to the user's system.
  # Which user downloaded which file at what time will be logged.
  # (adapted from http://wiki.rubyonrails.com/rails/pages/HowtoUploadFiles)
  def download
    # Log the 'usage' and return the file.
    send_file @myfile.path, :filename => @myfile.filename
  end

  # Shows upload progress.
  # For details, see http://mongrel.rubyforge.org/docs/upload_progress.html
  def progress
    render :update do |page|
      @status = Mongrel::Uploads.check(params[:upload_id])
      page.upload_progress.update(@status[:size], @status[:received]) if @status
    end
  end

  # Shows the form where a user can select a new file to upload.
  def upload
    @myfile = Myfile.new
    
    if USE_UPLOAD_PROGRESS
      render
    else
      render :template =>'file/upload_without_progress'
    end
  end

  # Upload the file and create a record in the database.
  # The file will be stored in the 'current' folder.
  def do_the_upload
    err = ''
    if( @myfile = process_each_file(params[:myfile], Time.now, params[:myfile_newname]) )
      if @myfile.filename =~ FILE_TYPES_BLOCKED
        flash[:folder_error] = "This file type is blocked and cannot be uploaded. Contact IT if you believe this is wrong."
        @myfile.log_usage("error","user tried to upload file with blocked filetype")
        @myfile.destroy
        redirect_to :controller => 'file', :action => 'upload', :id => folder_id and return false
      end

      @to_be_deleted = @myfile
      is_zip = true if params[:zip][:yes] == '1' || params[:zip_folders][:yes] == '1'
      zip_folders = true if params[:zip_folders][:yes] == '1'

      @folder = Folder.find(@myfile.folder_id)

      # general for all zip files - check if files exist/blocked filetypes
      if is_zip
        # check that no files of the wrong filetype and that file doesn't exist
        zf = Zip::ZipFile.open(@myfile.path)
        zf.each_with_index do |entry, index|
          next if entry.ftype != :file || entry.name =~ /^\.|\/\./ || entry.name == "__MACOSX"

          name = File.basename(Myfile.base_part_of(entry.name))
          if Myfile.find_by_filename_and_folder_id(name, @folder.id) || Folder.find_by_name_and_parent_id(name, @folder.parent_id)
            err = "The file #{name} has the same name as a file or folder in the current folder, and the zip file could not be extracted."
          end

          # if zip_folders, only check root files for conflicts
          next if entry.name.match(/\//) && zip_folders 
          if entry.name =~ FILE_TYPES_BLOCKED
            err = "The file #{name} is not an allowed file type, and the zip file could not be uploaded. Please contact IT if you believe this is wrong."
            @myfile.log_usage("error","user tried to upload file with blocked filetype #{entry.name}")
          end

          unless err.empty?
            @to_be_deleted.destroy
            flash[:folder_error] = err
            redirect_to :controller => 'file', :action => 'upload', :id => folder_id and return false
          end          
        end
        
        unless zip_folders    # we already know is_zip
          # all's clear, let's do this! :)
          zf.each_with_index do |entry, index|
            # don't add dirs as files, most people don't want dot files added
            # (Mac finder zip adds them)                                            
            next if entry.ftype != :file || entry.name =~ /^\.|\/\./ || entry.name == "__MACOSX"
            date_time_created = Time.now
            filesize = (entry.size / 1000).to_i
            filesize = 1 if filesize == 0
            # remove directory paths from the filename if they exist
            name = File.basename(entry.name)

            # extract one entry to a file in uploads/                                                       
            File.open("#{TEMP_PATH}/#{date_time_created.to_f}.tmp",   
            'wb') { |f| f.write(zf.file.open(entry.name).read) }
            #p "process #{name} #{date_time_created.to_f}"
            process_each_file(name, date_time_created, name, filesize)   

          end
    
        else  # zip folders
          # let's get the directories and make them first
          dirs, files = [], []
          zf.each_with_index do |entry, i|
            #p "entry name #{entry.name} ftype #{entry.ftype}"
            dirs << entry.name if entry.ftype == :directory
            files << entry if entry.ftype == :file
          end
      
          # # apparently not all dirs are listed as dir entries, so I have to do this. argh.
          # TODO: I took this out because it caused problems with normal zip files, but
          # there might still be some weird zip files which won't work
          # files.each do |f|
          #   dir, name = File.split(f.name)
          #   unless dirs.include?(dir)
          #     fulld = ''
          #     dir.split("/").each do |d|
          #       fulld += (fulld.empty? ? '' : "/") + d
          #       dirs << fulld + "/" unless dirs.include?(fulld)
          #     end
          #   end
          # end
      
          level, parent, dirs_created = 1, [], {}
          parent[level], old = @folder, @folder
        
          #p dirs
          # create new directories necessary
          dirs.sort.each do |dir|
            newlevel = dir.scan("/").length
            parent[newlevel] = old if newlevel > level
            level = newlevel
        
            dirname = dir.split("/").pop 

            old = make_new_folder(dirname, parent[level])
            dirs_created[dir] = old  
          end                      
      
          # extract files and put them in the new directories
          files.each do |file|
            dirname, filename = File.split(file.name)
            next if filename =~ /^\.|\/\./ || filename == "__MACOSX" || filename.nil?
            date_time_created = Time.now
            filesize = (file.size / 1000).to_i
            filesize = 1 if filesize == 0

            # remove directory paths from the filename if they exist
            name = File.basename(filename)
        
            # extract one entry to a file in TEMP_PATH                                                       
            File.open("#{TEMP_PATH}/#{date_time_created.to_f}.tmp", 'wb') do |f| 
              f.write(zf.file.open(file.name).read) 
            end
            #p dirs_created
            #p dirname
            dir = ( (dirname =~ /^\.$|^$/) ? @folder : dirs_created[dirname + "/"] )
            dir = @folder if dir.nil?
            process_each_file( name, date_time_created, name, filesize, dir.id )
        
          end
        end
        zf.close
        @to_be_deleted.destroy
      end
    
      redirect_to :controller => 'folder', :action => 'list', :id => folder_id
    else  # @myfile could not be saved
      # todo: check activerecord errors for other types of errors?
      flash[:folder_error] = "You cannot upload a file with the same name as a folder or a file that already exists."
      redirect_to :controller => 'file', :action => 'upload', :id => folder_id and return false
    end      
  end 
 
  def make_new_folder(folder, parent)   
    newfolder = Folder.new
    newfolder.name = folder
    newfolder.parent = parent
    newfolder.date_modified = Time.now
    newfolder.user = @logged_in_user
    newfolder.save
    copy_permissions_to_new_folder(parent, newfolder)
    return newfolder
  end

  def process_each_file(file, date_modified, name, size=0, folder=folder_id)
    @date_time_created=date_modified
    if file.kind_of?(String)
      @myfile = Myfile.new 
    else
      @myfile = Myfile.new(file) 
    end
    @myfile.filename = name
    @myfile.user = @logged_in_user
    @myfile.date_modified=date_modified if @myfile.date_modified.nil?
    @myfile.filename=Myfile.base_part_of(file) if file.kind_of?(String) # zip files
    @myfile.folder_id = folder
    @myfile.user = @logged_in_user
    @myfile.filesize=size if @myfile.filesize.nil?
    # change the filename if it already exists
    @myfile.save
    p @myfile
    return (@myfile.save ? @myfile : false)
  end

  # Copy the GroupPermissions of the parent folder to the given folder
  def copy_permissions_to_new_folder(oldfolder, folder)
    # get the 'parent' GroupPermissions
    for parent_group_permissions in GroupPermission.find_all_by_folder_id(oldfolder.id)
      # create the new GroupPermissions
      group_permissions = GroupPermission.new
      group_permissions.folder = folder
      group_permissions.group = parent_group_permissions.group
      group_permissions.can_create = parent_group_permissions.can_create
      group_permissions.can_read = parent_group_permissions.can_read
      group_permissions.can_update = parent_group_permissions.can_update
      group_permissions.can_delete = parent_group_permissions.can_delete
      group_permissions.save
    end
  end


  # Validates a selected file in a file field via an Ajax call
  def validate_filename
    filename = CGI::unescape(request.raw_post).chomp('=')
    filename = Myfile.base_part_of(filename)
    if Myfile.find_by_filename_and_folder_id(filename, folder_id).blank?
      render :text => %(<script type="text/javascript">document.getElementById('submit_upload').disabled=false;\nElement.hide('error');\nElement.hide('spinner');</script>)
    else
      render :text => %(<script type="text/javascript">document.getElementById('error').style.display='block';\nElement.hide('spinner');</script>\nThis file can not be uploaded, because it already exists in this folder.)
    end
  end

  # Show a form with the current name of the file in a text field.
  def rename
    render
  end

  # Update the name of the file with the new data.
  def update
    if request.post?
      if @myfile.rename(Myfile.base_part_of(params[:myfile][:filename]))
        redirect_to :controller => 'folder', :action => 'list', :id => folder_id
      else
        render_action 'rename'
      end
    end
  end

  # Preview file; possibly with highlighted search words.
  def preview
    if @myfile.indexed
      @myfile.log_usage("previewed")
      if params[:search].blank? # normal case
        @text = @myfile.text
      else # if we come from the search results page
        @text = @myfile.highlight(params[:search], { :field => :text, :excerpt_length => :all, :pre_tag => '[h]', :post_tag => '[/h]' })
      end
    end
  end

  # Delete a file.
  def destroy
    @myfile.destroy
    redirect_to :controller => 'folder', :action => 'list', :id => folder_id
  end

  # These methods are private:
  # [#does_file_exist] Check if a file exists before executing an action
  private
  # Check if a file exists before executing an action.
  # If it doesn't exist: redirect to 'list' and show an error message
  def does_file_exist
    @myfile = Myfile.find(params[:id])
  rescue
    flash.now[:folder_error] = 'Someone else deleted the file you are using. Your action was cancelled and you have been taken back to the root folder.'
    redirect_to :controller => 'folder', :action => 'list' and return false
  end
  
end