class Clipboard < ActiveRecord::Base
  belongs_to :myfile
  belongs_to :user
  belongs_to :folder

  # Initialize clipboard object.
  # We're starting with an empty clipboard:
  # the @folders and @files arrays are empty too.

  # Put given folder on clipboard
  # unless it's already there
  def add_folder(folder)
    @folders << folder unless @folders.find{ |f| f.id == folder.id }
  end

  # Put given file on clipboard
  # unless it's already there
  def add_file(file)
    @files << file unless @files.find{ |f| f.id == file.id }
  end
end