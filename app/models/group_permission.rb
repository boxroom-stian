# The Create/Read/Update/Delete permissions groups have
# on folders are stored in GroupPermissions
class GroupPermission < ActiveRecord::Base
  belongs_to :group
  belongs_to :folder

  # Create initial permissions.
  # The admins group get permission on the Root folder.
  # This method assumes the admins group exists and Root folder exists.
  def self.create_initial_permissions
    # Get the root folder and the admins group
    root_folder = Folder.find_by_is_root(true)
    admins_group = Group.find_by_is_the_administrators_group(true)

    # Create the permissions
    unless root_folder.blank? or admins_group.blank?
      %w(admins frontpage folderadmins users).each do |grpname|
        group = GroupPermission.new
        group.folder = root_folder
        group.group = Group.find_by_name(grpname)
        group.can_create = true
        group.can_read = true
        group.can_update = true
        group.can_delete = true
        group.save
      end
    end
  end
end