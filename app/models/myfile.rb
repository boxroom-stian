require 'zip/zipfilesystem'

# Files in the database are represented by Myfile.
# It's called Myfile, because File is a reserved word.
# Files are in (belong to) a folder and are uploaded by (belong to) a User.
class Myfile < ActiveRecord::Base
  acts_as_ferret :store_class_name => true, :fields => { :text => { :store => :yes }, :filename => { :store => :no } }

  belongs_to :folder
  belongs_to :user

  has_many :usages
  has_many :clipboards, :dependent => :destroy

  validates_uniqueness_of :filename, :scope => 'folder_id'

  # Validate if the user's data is valid.
  def validate
    if self.filename.blank?
      errors.add(:filename, " can't blank.")
    end
    if Folder.find_by_name_and_parent_id(self.filename, self.folder_id)
      errors.add_to_base("You cannot upload a file with the same name as a folder.")
    end
  end
  
  # Accessor that receives the data from the form in the view.
  # The file will be saved in a folder called 'uploads'.
  # (See: AWDWR pp. 362.)
  def myfile=(myfile_field)
    if myfile_field and myfile_field.length > 0
      # Get the filename
      filename = Myfile.base_part_of(myfile_field.original_filename)

      self.date_modified = Time.now

      # Save the file on the file system
      File.open(self.temp_path, 'wb') do |f|
        while buff = myfile_field.read(4096)
          f.write(buff)
        end
      end

      # Save it all to the database
      self.filename = filename
      filesize = (myfile_field.length / 1024).to_i
      if filesize == 0
        self.filesize = 1 # a file of 0 KB doesn't make sense
      else
        self.filesize = filesize
      end

    end
  end


  def index
    # Try to get the text from the uploaded file
    # Variable to hold the plain text content of the uploaded file
    text_in_file = nil
    filename = self.filename

    # Try the helpers first
    INDEX_HELPERS.each do |index_helper| # defined in environment.rb
      if filename =~ index_helper[:ext] # a matching helper!   

        if index_helper[:file_output] # a file that writes to an output file
          `#{ sprintf(index_helper[:helper], self.temp_path, self.temp_path + '_copy') }`
          if File.exists?(self.temp_path + '_copy')   # avoid error messages if external prog           
                                                      # borks
            text_in_file = File.open(self.temp_path + '_copy') { |f| f.read }
            File.delete(self.temp_path + '_copy')
          else
            text_in_file = ""
          end
        else # we get the contents from stido directly
          text_in_file = `#{ sprintf(index_helper[:helper], self.temp_path) }`
        end

        # Check if we need to remove first part (e.g. unrtf)
        unless index_helper[:remove_before].blank?
          if index_helper[:remove_before].match(text_in_file)
            text_in_file = Regexp::last_match.post_match 
          end
        end

        # Check if we need to remove last part
        unless index_helper[:remove_after].blank?
          if index_helper[:remove_after].match(text_in_file)
            text_in_file = Regexp::last_match.pre_match
          end
        end
      end
    end

    unless text_in_file # no hits yet - try the built in 
      case filename
        when /.txt$/
          text_in_file = File.open(self.temp_path) { |f| f.read }

        when /.htm$|.html$/ # get the file, strip all <> tags
          text_in_file = File.open(self.temp_path) { |f| f.read.gsub(/<head>.*?<\/head>/m,'').gsub(/<.*?>/, ' ') }

        when /.sxw$|.odt$/ # read content.xml from zip file, strip <> tags
          Zip::ZipFile.open(self.temp_path) do |zipfile|
            text_in_file = zipfile.file.open('content.xml') { |f| f.read.gsub(/<.*?>/, ' ') }
          end
      end
    end
    
    if text_in_file && !text_in_file.strip.empty?
      self.text = text_in_file.strip # assign text_in_file to self.text to get it indexed
      self.indexed = true
      self.save
    end
  end


  attr_writer :text # Setter for text

  # Getter for text.
  # If text is blank get the text from the index.
  def text
    @text = Myfile.ferret_index[self.document_number][:text] if @text.blank?
  end

  after_create :index, :rename_newfile
  # The file in the uploads folder has the same name as the id of the file.
  # This must be done after_create, because the id won't be available any earlier.
  def rename_newfile
    File.rename self.temp_path, self.path
    log_usage("uploaded")
  end

  before_destroy :delete_file_on_disk
  # When removing a myfile record from the database,
  # the actual file on disk has to be removed too.
  # However, instead of deleting, we move it to the trash directory. Safer.
  def delete_file_on_disk 
    if File.exists? self.path
      new_name = "#{TRASH_PATH}/#{basename}.#{Time.now.to_f.to_s}"
      log_usage("deleted","moved from #{self.path} to #{new_name}")
      File.mv(self.path, new_name)
    end    
  end
  
  def rename(filename)
    old_filename = self.filename
    if self.update_attributes(:filename => filename, :date_modified => Time.now) && File.rename( self.folder.path_on_disk + "/" + old_filename, self.path )
    
      log_usage("renamed","from #{old_filename} to #{self.filename}")
    else
      return false
    end
  end
    
  # Strip of the path and replace all the non alphanumeric,
  # underscores and periods in the filename with an underscore.
  def self.base_part_of(file_name)
    # NOTE: File.basename doesn't work right with Windows paths on Unix
    # INCORRECT: just_filename = File.basename(file_name.gsub('\\\\', '/')) 
    # get only the filename, not the whole path
    name = file_name.gsub(/^.*(\\|\/)/, '')

    # finally, replace all non alphanumeric, underscore or periods with space, and
    # reduce all spaces to maximum one, with no trailing or leading
    name.gsub(/[^\w\.\-]/, ' ').gsub(/([^\s])(\s+?)([^\s])/,'\1 \3').gsub(/([^\s])(\s+?)([^\s])/,'\1 \3').strip
  end

  def basename
    return Myfile.base_part_of(self.filename)
  end

  # Returns the location of the file before it's saved
  def temp_path
    "#{TEMP_PATH}/#{self.date_modified.to_f}.tmp" 
  end

  # The path of the file
  def path
    File.join(self.folder.path_on_disk, basename)
  end
  
  def icon_file
    FILE_ICONS.each do |f_icon|
      if self.filename =~ f_icon[:ext]
        return f_icon[:icon]
      end
    end
    return 'file.png'
  end
  
  def short_fname(length = 50)
    return self.filename.shorten(length)
  end
     
  def log_usage(action, comment = nil)
    usage = Usage.new(
      :download_date_time => Time.now,
      :user => User.logged_in_user,
      :myfile => self,
      :filename => self.filename,
      :action => action,
      :comment => comment 
      ).save
  end
     
     
  private
  def read_file(file)
    File.open(file) { |f| return f.read }
  end  
      
end