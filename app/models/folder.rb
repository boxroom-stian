# A folder is a place where files can be stored.
# Folders can also have sub-folders.
# Via groups it is determined which actions the logged-in User can perform.
class Folder < ActiveRecord::Base
  acts_as_ferret :store_class_name => true, :fields => { :name => { :store => :no } }
  acts_as_tree :order => :name

  belongs_to :user
  has_many :myfiles, :dependent => :destroy
  has_many :group_permissions, :dependent => :destroy
  has_many :clipboards, :dependent => :destroy
  has_many :usages

  validates_uniqueness_of :name, :scope => 'parent_id'
  validates_presence_of :name

  cattr_accessor :clipboard
  attr_accessible :name

  def after_create
    # create the physical folder on disk
    Dir.mkdir(self.path_on_disk) unless File.exists?(self.path_on_disk)
  end
  
  # have to call destroy through this, otherwise the moving doesn't work properly
  def delete
    trash_name = "#{TRASH_PATH}/#{name}.#{Time.now.to_f.to_s}"
    File.mv(path_on_disk, trash_name)
    p "Moving from #{path_on_disk} to #{trash_name}"
    destroy
  end

  # gets called after delete, so folder has already been moved
  def before_destroy 
    trash_name = "#{TRASH_PATH}/#{@name}.#{Time.now.to_f.to_s}"
    log_usage("deleted","moved from01 #{self.path_on_disk} moved to #{trash_name}")
  end

  def path_on_disk
    folder = self
    path = folder.name
    unless folder.parent_id == 0
      until folder.parent_id == 1 
        folder = folder.parent
        path = folder.name + "/" + path
      end
    else
      path = ""
    end
    return (UPLOAD_PATH + "/" + path)
  end  
  
  def rename(name)
    old_name = self.name
    parent_path = self.parent.path_on_disk
    if self.update_attributes(:name => Myfile.base_part_of(name), :date_modified => Time.now) && File.rename( parent_path + "/" + old_name, self.path_on_disk)
      log_usage("renamed","from #{old_name} to #{self.name}")
    else
      return false
    end
  end
  
  def note_inherited
    return self.note unless self.note.nil? || self.note.empty?
    current, result = self, ''
    until !result.empty? || current == nil
      if !current.note.nil? && !current.note.empty? && current.note_inheritable
        result = current.note
      else
        current = current.parent
      end
    end
    return (result.empty? ? nil : result)
  end
  
  def note_upload_inherited
    return self.note_upload unless self.note_upload.nil? || self.note_upload.empty?
    current, result = self, ''
    until !result.empty? || current == nil
      if !current.note_upload.nil? && !current.note_upload.empty? && current.note_upload_inheritable
        result = current.note_upload
      else
        current = current.parent
      end
    end
    return (result.empty? ? nil : result)
  end

  # List subfolders
  # for the given user in the given order.
  def list_subfolders(logged_in_user, order)
    folders = []
    if logged_in_user.can_read(self.id)
      self.children.each do |sub_folder|
        folders << sub_folder if logged_in_user.can_read(sub_folder.id)
      end
    end

    # return the folders:
    return folders
  end

  # List the files
  # for the given user in the given order.
  def list_files(logged_in_user, order)
    files = []
    if logged_in_user.can_read(self.id)
      files = self.myfiles.find(:all, :order => order)
    end

    # return the files:
    return files
  end

  # Returns whether or not the root folder exists
  def self.root_folder_exists?
    folder = Folder.find_by_is_root(true)
    return (not folder.blank?)
  end

  # Create the Root folder
  def self.create_root_folder
    if User.admin_exists? #and Folder.root_folder_exists?
      folder = self.new
      folder.name = 'Root folder'
      folder.date_modified = Time.now
      folder.is_root = true
      folder.lft, folder.rgt = 1, 2 # must be initialized, otherwise quota check on upload
                                    # won't work until another folder has been added

      # This folder is created by the admin
      if user = User.find_by_is_the_administrator(true)
        folder.user = user
      end

      folder.save # this hopefully returns true
    end
  end

  def log_usage(action, comment = nil)
    usage = Usage.new(
      :download_date_time => Time.now,
      :user => User.logged_in_user,
      :folder => self,
      :filename => self.name,
      :action => action,
      :comment => comment 
      ).save
  end
                 
  def all_with_children
    folders = [self] 
    self.children.each do |child_folder|
      folders += child_folder.all_with_children if child_folder.children
    end          
    return folders
  end

  def all_children
    self.all_with_children - [self]
  end


  private
  def validate
    if Myfile.find_by_filename_and_folder_id(self.name, self.parent_id)
      errors.add_to_base "You cannot create a folder with the same name as a file."
    end
  end
  
end