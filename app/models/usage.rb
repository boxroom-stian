# Usage contains details about which user downloaded
# which file at what time
class Usage < ActiveRecord::Base
  belongs_to :user
  belongs_to :myfile
  belongs_to :folder

  def self.log_usage(options) 
    usage = Usage.new(:download_date_time => Time.now)
    %w(file folder comment user action).each do |x| 
      eval("usage.#{x} = options[:#{x}]") if options[x]
    end
    return usage.save
  end   
end